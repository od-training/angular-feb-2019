import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Video } from './dashboard/types';

@Injectable({
  providedIn: 'root'
})
export class VideoDataService {

  constructor(private http: HttpClient) { }

  loadVideos() {
    return this.http.get<Video[]>('http://api.angularbootcamp.com/videos').pipe(
      map(videos => videos.slice(0, 4))
    );
  }

  getVideo(id: string) {
    return this.http.get<Video>('http://api.angularbootcamp.com/videos/' + id);
  }
}
