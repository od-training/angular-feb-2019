import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-stat-filters',
  templateUrl: './stat-filters.component.html',
  styleUrls: ['./stat-filters.component.css']
})
export class StatFiltersComponent implements OnInit {
  statsForm: FormGroup;

  constructor(fb: FormBuilder) {
    this.statsForm = fb.group({
      stats: [''],
      moreStats: ['']
    });
   }

  ngOnInit() {
  }
  submitForm() {
    console.log(this.statsForm.value);
  }
}
