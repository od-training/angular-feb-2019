import { Component, OnInit, Input } from '@angular/core';
import { Video } from '../types';
import { ActivatedRoute, Params } from '@angular/router';
import { Observable } from 'rxjs';
import { VideoDataService } from 'src/app/video-data.service';
import { pluck, switchMap, share, tap, filter } from 'rxjs/operators';
import { SafeResourceUrl, DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-video-player',
  templateUrl: './video-player.component.html',
  styleUrls: ['./video-player.component.css']
})
export class VideoPlayerComponent implements OnInit {
  video: Observable<Video>;
  videoUrl: SafeResourceUrl | undefined;

  constructor(route: ActivatedRoute, svc: VideoDataService, domSanitizer: DomSanitizer) {
    this.video = route.queryParams.pipe(
      pluck<Params, string>('id'),
      filter(id => !!id),
      switchMap(id => svc.getVideo(id).pipe(
        tap(video => this.videoUrl = domSanitizer.bypassSecurityTrustResourceUrl(`https://www.youtube.com/embed/${video.id}`))
      )),
      share()
    );
  }

  ngOnInit() {
  }

}
