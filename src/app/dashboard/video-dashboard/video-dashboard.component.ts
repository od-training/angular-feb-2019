import { Component, OnInit } from '@angular/core';
import { Video } from '../types';
import { HttpClient } from '@angular/common/http';
import { VideoDataService } from 'src/app/video-data.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-video-dashboard',
  templateUrl: './video-dashboard.component.html',
  styleUrls: ['./video-dashboard.component.css']
})
export class VideoDashboardComponent implements OnInit {
  selectedVideo: Video;
  videos: Observable<Video[]>;

  constructor(svc: VideoDataService) {
    this.videos = svc.loadVideos();
  }

  ngOnInit() {
  }

  chooseVideo(video: Video) {
    this.selectedVideo = video;
    console.log('video', video);
  }
}
